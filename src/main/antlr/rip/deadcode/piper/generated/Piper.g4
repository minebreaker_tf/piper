grammar Piper;

@header {
package rip.deadcode.piper.generated;
import rip.deadcode.piper.Slf4jErrorListener;
}

// 初期化子を使い、パーサー/レクサーのインスタンス作成時にリスナーをセットする。
// http://stackoverflow.com/questions/11194458/forcing-antlr-to-use-my-custom-treeadaptor-in-a-parser
@members {
{
    this.removeErrorListeners();
    this.addErrorListener(Slf4jErrorListener.INSTANCE);
}
}

CONTINUOUS_NEWLINE
    : '\r'? '\n' ' ' -> skip
    ;

NEWLINE
    : ('\r'? '\n')+
    ;

WS
    : (' ' | '\t') -> skip
    ;

IDENTIFIER
    : [a-zA-Z_]+
    ;

DECIMAL
    : '-'? [0-9]+
    ;

FLOAT
    : '-'? [0-9]+ '.' [0-9]+
    ;

STRING
    : '"' .*? '"'
    ;

BOOLEAN
    : 'true'
    | 'false'
    ;

PIPE
    : '|>'
    ;

ARROW
    : '->'
    ;

file
    : triggerStatement bindingStatement*
    ;

triggerStatement
    : trigger PIPE pipeline PIPE output NEWLINE
    ;

trigger
    : 'trigger' IDENTIFIER attribute*
    ;

attribute
    : IDENTIFIER '=' value
    ;

pipeline
    : pipe (PIPE pipe)*
    ;

pipe
    : lambda
    | IDENTIFIER
    ;

output
    : IDENTIFIER attribute*
    ;

bindingStatement
    : IDENTIFIER ':' expression NEWLINE
    ;

expression
    : lambda
    | bracedExpression
    | value
    | functionInvocation
    | ifExpression
    | <assoc=left> expression comparisonOperator expression
    | <assoc=left> expression operator expression
    | <assoc=left> expression secondaryOperator expression
    | binding
    ;

bracedExpression
    : '(' expression ')'
    ;

lambda
    : '\\' parameters? ARROW expression
    ;

parameters
    : IDENTIFIER (',' IDENTIFIER)*
    ;

functionInvocation
    : IDENTIFIER '(' arguments? ')'
    ;

arguments
    : expression (',' expression)*
    ;

ifExpression
    : 'if' expression 'then' expression 'else' expression
    ;

comparisonOperator
    : '=='
    | '<>'
    | '<'
    | '<='
    | '>'
    | '>='
    ;

operator
    : '*'
    | '/'
    ;

secondaryOperator
    : '+'
    | '-'
    ;

value
    : DECIMAL
    | FLOAT
    | STRING
    | booleanLiteral
    ;

booleanLiteral
    : 'true'
    | 'false'
    ;

binding
    : IDENTIFIER
    ;
