package rip.deadcode.piper

import com.google.gson.Gson
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

typealias DefaultFunction = (List<Any?>) -> Any?

val embeddedFunctions = mapOf<String, DefaultFunction>(
        "now" to ::now,
        "list" to ::list,
        "map" to ::map,
        "getValue" to ::getValue,
        "parseJson" to ::parseJson,
        "stringifyJson" to ::stringifyJson
)

internal fun now(@Suppress("UNUSED_PARAMETER") args: List<Any?>): Any? =
        LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME)

internal fun list(args: List<Any?>): Any? =
        args

internal fun map(args: List<Any?>): Any? =
        if (args.isEmpty()) {
            mapOf()
        } else {
            args.windowed(2, 2)
                    .map { list -> Pair(list[0], list[1]) }
                    .toMap()
        }

internal fun getValue(args: List<Any?>): Any? {
    val collection = args[0]
    val key = args[1]
    return when (collection) {
        is List<*> -> {
            val ind = when (key) {
                is Int -> key
                is Long -> key.toInt()
                else -> key.toString().toInt()
            }
            collection[ind]
        }
        is Map<*, *> -> collection[key]
        null -> null
        else -> throw RuntimeException()
    }
}

internal val gson = Gson()

internal fun parseJson(args: List<Any?>): Any? {
    val input = args[0]
    return if (input is String) {
        gson.fromJson(input, Map::class.java)
    } else throw RuntimeException()
}

internal fun stringifyJson(args: List<Any?>): Any? =
        gson.toJson(args[0])
