package rip.deadcode.piper

import rip.deadcode.piper.generated.PiperParser

fun interpret(tree: PiperParser.PipelineContext, eventObject: Any?, bindings: Bindings): Any? =
        interpret(tree.pipe(), eventObject, bindings)

internal fun interpret(ctx: List<PiperParser.PipeContext>, upstreamResult: Any?, bindings: Bindings): Any? {

    val pipe = ctx[0]
    val id = pipe.IDENTIFIER()?.text

    val result = if (embeddedFunctions.containsKey(id)) {
        embeddedFunctions[id]!!(listOf(upstreamResult))

    } else {
        val f = if (id != null) {
            bindings[id] as PiperParser.LambdaContext
        } else {
            pipe.lambda()
        }
        val currentBindings = if (f.parameters() != null && f.parameters().IDENTIFIER().isNotEmpty()) {
            bindings + (f.parameters().IDENTIFIER()[0].text to upstreamResult)
        } else {
            bindings
        }
        eval(f.expression(), currentBindings)
    }

    return if (ctx.size >= 2) {
        interpret(ctx.drop(1), result, bindings)
    } else {
        result
    }
}

fun eval(expr: PiperParser.ExpressionContext, bindings: Bindings): Any? {
    return when {
        expr.bracedExpression() != null -> evalBracedExpression(expr.bracedExpression(), bindings)
        expr.operator() != null -> evalOperativeExpression(expr, bindings)
        expr.comparisonOperator() != null -> evalComparativeExpression(expr, bindings)
        expr.secondaryOperator() != null -> evalSecondaryOperativeExpression(expr, bindings)
        expr.functionInvocation() != null -> evalFunctionInvocation(expr.functionInvocation(), bindings)
        expr.value() != null -> evalValue(expr.value())
        expr.lambda() != null -> expr.lambda()
        expr.ifExpression() != null -> evalIf(expr.ifExpression(), bindings)
        expr.binding() != null -> bindings[expr.binding().IDENTIFIER().text]
        else -> throw RuntimeException()
    }
}

fun evalBracedExpression(expr: PiperParser.BracedExpressionContext, bindings: Bindings): Any? =
        eval(expr.expression(), bindings)

fun evalComparativeExpression(expr: PiperParser.ExpressionContext, bindings: Bindings): Any {

    val op = expr.comparisonOperator().text
    val left = eval(expr.expression(0), bindings)
    val right = eval(expr.expression(1), bindings)
    return if (op == "==") {
        left == right
    } else if (op == "<>") {
        left != right
    } else if (left is Number && right is Number) {
        val l = left.toDouble()
        val r = right.toDouble()
        when (op) {
            "<=" -> l <= r
            "<" -> l < r
            ">=" -> l >= r
            ">" -> l > r
            else -> throw RuntimeException()
        }
    } else throw RuntimeException()
}

fun evalOperativeExpression(expr: PiperParser.ExpressionContext, bindings: Bindings): Any {

    val op = expr.operator().text
    val left = eval(expr.expression(0), bindings)
    val right = eval(expr.expression(1), bindings)
    return if (op == "*") {
        if (left is Long && right is Long) {
            left * right
        } else if (left is Number && right is Number) {
            left.toDouble() * right.toDouble()
        } else throw RuntimeException()
    } else if (op == "/") {
        if (left is Number && right is Number) {
            left.toDouble() / right.toDouble()
        } else throw RuntimeException()
    } else throw RuntimeException()
}

fun evalSecondaryOperativeExpression(expr: PiperParser.ExpressionContext, bindings: Bindings): Any {

    val op = expr.secondaryOperator().text
    val left = eval(expr.expression(0), bindings)
    val right = eval(expr.expression(1), bindings)
    return if (op == "+") {
        if (left is Long && right is Long) {
            left + right
        } else if (left is Number && right is Number) {
            left.toDouble() + right.toDouble()
        } else if (left is String) {
            left.toString() + right.toString()
        } else throw RuntimeException()
    } else if (op == "-") {
        if (left is Long && right is Long) {
            left - right
        } else if (left is Number && right is Number) {
            left.toDouble() - right.toDouble()
        } else throw RuntimeException()
    } else throw RuntimeException()
}

fun evalFunctionInvocation(expr: PiperParser.FunctionInvocationContext, bindings: Bindings): Any? {

    val id = expr.IDENTIFIER().text
    val f = bindings[id]!!
    val args = expr
            .arguments()
            ?.expression()
            ?.map { eval(it, bindings) }
            ?: listOf()

    return when {
        embeddedFunctions.containsKey(id) -> {
            embeddedFunctions[id]!!(args)
        }
        f is PiperParser.LambdaContext -> {
            val currentBindings = bindings + (f.parameters().IDENTIFIER().map { it.text } zip args)

            eval(f.expression(), currentBindings)
        }
        else -> throw RuntimeException()
    }
}

fun evalIf(expr: PiperParser.IfExpressionContext, bindings: Bindings): Any? {

    val cond = eval(expr.expression()[0]!!, bindings)

    return if (cond != false && cond != null) {
        eval(expr.expression()[1]!!, bindings)
    } else {
        eval(expr.expression()[2]!!, bindings)
    }
}

fun evalValue(value: PiperParser.ValueContext): Any {

    return when {
        value.DECIMAL() != null -> value.DECIMAL().text.toLong()
        value.FLOAT() != null -> value.FLOAT().text.toDouble()
        value.STRING() != null -> value.STRING().text.toString().drop(1).dropLast(1)
        value.booleanLiteral() != null -> value.booleanLiteral().text!!.toBoolean()
        else -> throw RuntimeException()
    }
}
