package rip.deadcode.piper.output

import rip.deadcode.piper.evalValue
import rip.deadcode.piper.generated.PiperParser
import java.nio.file.Files
import java.nio.file.Paths

fun processOutput(result: Any?, output: PiperParser.OutputContext) {

    val id = output.IDENTIFIER().text
    val attrs = output.attribute()
            .map { it.IDENTIFIER().text to evalValue(it.value()) }
            .toMap()
    when (id) {
        "Stdout" -> stdout(result)
        "File" -> file(result, attrs)
        else -> throw RuntimeException()
    }
}

fun stdout(result: Any?) {
    println(result.toString())
}

fun file(result: Any?, attrs: Map<String, Any>) {

    val fileName = attrs["name"]!!.toString()
    val dest = Paths.get(fileName)
    if (result is ByteArray) {
        Files.write(dest, result)
    } else if (result != null) {
        Files.write(dest, result.toString().toByteArray())
    }
}
