package rip.deadcode.piper

import org.antlr.v4.runtime.BufferedTokenStream
import org.antlr.v4.runtime.CharStreams
import rip.deadcode.piper.generated.PiperLexer
import rip.deadcode.piper.generated.PiperParser
import rip.deadcode.piper.trigger.getInitializer

fun main(args: Array<String>) {

    val sourceFile = args[0]
    val lexer = PiperLexer(CharStreams.fromFileName(sourceFile))
    val parser = PiperParser(BufferedTokenStream(lexer))

    val tree = parser.file()
    interpret(tree)
}

fun interpret(file: PiperParser.FileContext) {

    val trigger = file.triggerStatement().trigger()
    val triggerId = trigger.IDENTIFIER().text
    val attrs = trigger.attribute()
            .map { it.IDENTIFIER().text to evalValue(it.value()) }
            .toMap()

    val initialize = getInitializer(triggerId)
    val bindings = bind(file.bindingStatement())
    val output = file.triggerStatement().output()

    initialize(file.triggerStatement().pipeline(), output, attrs, bindings)
}

typealias Initializer = (PiperParser.PipelineContext, PiperParser.OutputContext, Map<String, Any>, Bindings) -> Unit

typealias Bindings = Map<String, Any?>

fun bind(tokenBindings: List<PiperParser.BindingStatementContext>): Bindings =
        embeddedFunctions + if (tokenBindings.isEmpty()) {
            mapOf()
        } else {
            tokenBindings.map { bind(it) }.reduce { acc, current -> acc + current }
        }

internal fun bind(bindingStatement: PiperParser.BindingStatementContext): Bindings =
        mapOf(bindingStatement.IDENTIFIER().text to eval(bindingStatement.expression(), embeddedFunctions))
