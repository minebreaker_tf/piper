package rip.deadcode.piper.trigger

import com.google.common.base.Preconditions.checkState
import com.google.common.io.CharStreams
import org.eclipse.jetty.server.Request
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.handler.AbstractHandler
import rip.deadcode.piper.Bindings
import rip.deadcode.piper.generated.PiperParser
import rip.deadcode.piper.interpret
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

fun invokeHttp(
        tree: PiperParser.PipelineContext,
        output: PiperParser.OutputContext,
        attrs: Map<String, Any?>,
        bindings: Bindings) {

    val port = attrs["port"]?.toString()?.toInt() ?: 8080
    val method = attrs["method"]?.toString()?.toLowerCase() ?: "get"
    val url = attrs["url"]?.toString()
    checkState(output.IDENTIFIER().text == "Http")

    val server = Server(port)
    server.handler = object : AbstractHandler() {
        override fun handle(target: String, baseRequest: Request, request: HttpServletRequest, response: HttpServletResponse) {
            val body = if (url == target && baseRequest.method.toLowerCase() == method) {
                val input = mapOf(
                        "url" to target,
                        "queryParam" to baseRequest.queryParameters,
                        "body" to CharStreams.toString(request.reader)
                )
                val result = interpret(tree, input, bindings)
                result.toString()
            } else {
                "<h1>404 Not Found</h1>"
            }
            response.contentType = "text/html; charset=utf-8"
            response.status = HttpServletResponse.SC_OK
            response.outputStream.print(body)
            baseRequest.isHandled = true
        }
    }

    server.start()
    server.join()
}
