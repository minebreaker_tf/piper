package rip.deadcode.piper.trigger

import rip.deadcode.piper.Bindings
import rip.deadcode.piper.generated.PiperParser
import rip.deadcode.piper.interpret
import rip.deadcode.piper.output.processOutput

fun invokeOnce(
        tree: PiperParser.PipelineContext,
        output: PiperParser.OutputContext,
        @Suppress("UNUSED_PARAMETER") attrs: Map<String, Any?>,
        bindings: Bindings) {

    val result = interpret(tree, null, bindings)
    processOutput(result, output)
}
