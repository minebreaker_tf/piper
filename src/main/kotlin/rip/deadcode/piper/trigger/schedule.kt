package rip.deadcode.piper.trigger

import it.sauronsoftware.cron4j.Scheduler
import rip.deadcode.piper.Bindings
import rip.deadcode.piper.generated.PiperParser
import rip.deadcode.piper.interpret
import rip.deadcode.piper.output.processOutput

fun invokeSchedule(
        tree: PiperParser.PipelineContext,
        output: PiperParser.OutputContext,
        attrs: Map<String, Any?>,
        bindings: Bindings) {

    val pattern = attrs["pattern"].toString()

    val s = Scheduler()
    s.schedule(pattern) {
        val result = interpret(tree, pattern, bindings)
        processOutput(result, output)
    }
    s.start()
}
