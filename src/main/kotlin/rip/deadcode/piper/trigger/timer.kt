package rip.deadcode.piper.trigger

import rip.deadcode.piper.Bindings
import rip.deadcode.piper.generated.PiperParser
import rip.deadcode.piper.interpret
import rip.deadcode.piper.output.processOutput

fun invokeTimer(
        tree: PiperParser.PipelineContext,
        output: PiperParser.OutputContext,
        attrs: Map<String, Any?>,
        bindings: Bindings) {

    val interval = attrs["interval"]!!.toString().toLong()

    while (true) {
        val result = interpret(tree, interval, bindings)
        processOutput(result, output)

        Thread.sleep(interval)
    }
}
