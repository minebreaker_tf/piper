package rip.deadcode.piper.trigger

import rip.deadcode.piper.Initializer

fun getInitializer(triggerId: String): Initializer =

        when (triggerId) {
            "Once" -> ::invokeOnce
            "Timer" -> ::invokeTimer
            "Schedule" -> ::invokeSchedule
            "Http" -> ::invokeHttp
            else -> throw RuntimeException("Unknown trigger: ${triggerId}")
        }
