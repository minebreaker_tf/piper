package rip.deadcode.piper

import com.google.common.truth.Truth.assertThat
import org.junit.jupiter.api.Test
import java.time.format.DateTimeFormatter

internal class FunctionsKtTest {

    @Test
    fun testNow() {
        val result = now(listOf()).toString()

        // Can be successfully parsed
        DateTimeFormatter.ISO_DATE_TIME.parse(result)
    }

    @Test
    fun testList() {
        var result = list(listOf())
        assertThat(result as List<*>).isEmpty()

        result = list(listOf(1, 2L, "3"))
        assertThat(result as List<*>).containsExactly(1, 2L, "3")
    }

    @Test
    fun testMap() {
        var result = map(listOf())
        assertThat(result as Map<*, *>).isEmpty()

        result = map(listOf("hoge", 1, "piyo", "fuga"))
        assertThat(result as Map<*, *>).containsExactly("hoge", 1, "piyo", "fuga")
    }

    @Test
    fun testGetValue() {

        var result = getValue(listOf(listOf(1, 2, 3), 1))
        assertThat(result).isEqualTo(2)

        result = getValue(listOf(mapOf("hoge" to 1, "piyo" to 2), "piyo"))
        assertThat(result).isEqualTo(2)
    }

    @Test
    fun testParseJson() {

        val result = parseJson(listOf("""
            {"hoge": "piyo"}
        """.trimIndent()))

        assertThat(result as Map<*, *>).containsExactly("hoge", "piyo")
    }

    @Test
    fun testStringifyJson() {

        val input = map(listOf("hoge", 1, "piyo", listOf(2, 3, 4)))
        val result = stringifyJson(listOf(input))

        assertThat(result).isEqualTo("""{"hoge":1,"piyo":[2,3,4]}""")
    }
}
