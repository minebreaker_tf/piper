package rip.deadcode.piper

import com.google.common.truth.Truth.assertThat
import org.antlr.v4.runtime.BufferedTokenStream
import org.antlr.v4.runtime.CharStreams
import org.junit.jupiter.api.Test
import rip.deadcode.piper.generated.PiperLexer
import rip.deadcode.piper.generated.PiperParser

internal class InterpreterKtTest {

    private fun parse(src: String): PiperParser.ExpressionContext {
        val lexer = PiperLexer(CharStreams.fromString(src))
        val parser = PiperParser(BufferedTokenStream(lexer))
        return parser.expression()
    }

    @Test
    fun testEval() {
        var result = eval(parse("1 + 2 * 3"), mapOf())
        assertThat(result).isEqualTo(7)

        result = eval(parse("(1 + 2) * 3"), mapOf())
        assertThat(result).isEqualTo(9)

        result = eval(parse("1 * 2 + 3"), mapOf())
        assertThat(result).isEqualTo(5)

        result = eval(parse("2 + 3 * 1 + 2 - 10 / 2"), mapOf())
        assertThat(result as Double).isWithin(0.01).of(2.0)

        result = eval(parse("(1 + 2) - (3 + 4)"), mapOf())
        assertThat(result).isEqualTo(-4)

        result = eval(parse("true == true"), mapOf())
        assertThat(result).isEqualTo(true)

        result = eval(parse("true <> false"), mapOf())
        assertThat(result).isEqualTo(true)

        result = eval(parse("0 < 1"), mapOf())
        assertThat(result).isEqualTo(true)

        result = eval(parse("0 <= 1"), mapOf())
        assertThat(result).isEqualTo(true)

        result = eval(parse("0 <= 0"), mapOf())
        assertThat(result).isEqualTo(true)

        result = eval(parse("0 > 1"), mapOf())
        assertThat(result).isEqualTo(false)

        result = eval(parse("0 >= 1"), mapOf())
        assertThat(result).isEqualTo(false)

        result = eval(parse("0 >= 0"), mapOf())
        assertThat(result).isEqualTo(true)

        result = eval(parse("if 0 == 0 then true else false"), mapOf())
        assertThat(result).isEqualTo(true)

        result = eval(parse("if 0 <> 0 then true else false"), mapOf())
        assertThat(result).isEqualTo(false)
    }
}
