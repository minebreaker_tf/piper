package rip.deadcode.piper

import com.google.common.truth.Truth.assertThat
import org.antlr.v4.runtime.BufferedTokenStream
import org.antlr.v4.runtime.CharStreams
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import rip.deadcode.piper.generated.PiperLexer
import rip.deadcode.piper.generated.PiperParser
import java.io.ByteArrayOutputStream
import java.io.PrintStream
import java.nio.charset.StandardCharsets

internal class RunnerKtTest {

    private var os = ByteArrayOutputStream()

    @BeforeEach
    fun setUp() {
        os = ByteArrayOutputStream()
        System.setOut(PrintStream(os))
    }

    private fun readOutputs() = os.toByteArray().toString(StandardCharsets.UTF_8)

    private fun parse(src: String): PiperParser.FileContext {

        val lexer = PiperLexer(CharStreams.fromString(src))
        val parser = PiperParser(BufferedTokenStream(lexer))
        return parser.file()
    }

    @Test
    fun test1() {

        val src = """trigger Once |> \ -> "hello, world" |> Stdout
"""
        interpret(parse(src))
        assertThat(readOutputs().trim()).isEqualTo("hello, world")

    }

    @Test
    fun test2() {

        val src = """trigger Once |> calc |> Stdout

calc: \ -> 4 + 3 * 2 - 4 / 2
"""
        interpret(parse(src))
        assertThat(readOutputs().trim()).isEqualTo("8.0")
    }

    @Test
    fun test3() {

        val src = """trigger Once |> \ -> "John" |> greet |> Stdout

message: "Hello, "
greet: \ name -> message + name + "!"
"""
        interpret(parse(src))
        assertThat(readOutputs().trim()).isEqualTo("Hello, John!")
    }
}
